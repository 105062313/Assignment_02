var leftWall;
var rightWall;
var ceiling;
var database;
var bg;
var menuState = {
    preload: function () {
        game.load.image('bg','background.jpg');
        game.load.audio('bg','bg.ogg');

    },
    create: function () {
        game.add.image(0, 0, 'bg');
        bg=game.add.audio('bg');
        bg.loop = true;
        bg.volume = 0.5;
        bg.play();
        var startLabel1 = game.add.text(game.width/2, game.height-80,'1p game<press"z">', { font: '20px Arial', fill: '#ffffff' }); 
        startLabel1.anchor.setTo(0.5, 0.5);
        var startLabel2 = game.add.text(game.width/2, game.height-50,'2p game<press"x">', { font: '20px Arial', fill: '#ffffff' }); 
        startLabel2.anchor.setTo(0.5, 0.5);
        var startLabel4 = game.add.text(game.width/2, game.height-20,'(2p use "z","x"to control)', { font: '15px Arial', fill: '#ffffff' }); 
        startLabel4.anchor.setTo(0.5, 0.5);
        
        

        var startLabel3 = game.add.text(game.width/2, game.height-350,'TOP 5', { font: '30px Arial', fill: '#ffffff' }); 
        startLabel3.anchor.setTo(0.5, 0.5);
        var scoreLabel = game.add.text(game.width/2, game.height-220,'........', { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        database = firebase.database().ref().orderByChild('order').limitToFirst(5);
        database.once('value', function(snapshot){
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                scoreText += childSnapshot.val().name + ' :' + childSnapshot.val().score + '\n';
            });
            scoreLabel.text = scoreText;
        });
    

        var ZKey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        ZKey.onDown.add(this.start, this);
        var XKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
        XKey.onDown.add(this.start2, this);
        

    },
    start: function () {
        bg.stop();
        status='running';
        distance=0;
        game.state.start('play');
    },
    start2: function () {
        bg.stop();
        status2='running';
        game.state.start('play2');
    }
};