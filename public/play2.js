var children1;
var children2;
var keyboard;
var children;


var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text4;
var text5;
var text6;

var distance = 0;
var status2 = 'running';







var playState2={
    preload:function  () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('children1', 'player.png', 32, 32);
        game.load.spritesheet('children2', 'player2.png', 32, 32);
        game.load.image('wall', 'wall.png');
        game.load.image('ceiling', 'ceiling.png');
        game.load.image('normal', 'normal.png');
        game.load.image('nails', 'nails.png');
        game.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'fake.png', 96, 36);
        game.load.audio('hurt','hurt.wav');
        game.load.audio('jump','jump.ogg');
        game.load.audio('die','die.wav');
    },
    create:function  () {

        keyboard = game.input.keyboard.addKeys({
            'esc': Phaser.Keyboard.ESC,
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'z': Phaser.Keyboard.Z,
            'x': Phaser.Keyboard.X,
        });
        
        bg.loop = true;
        bg.volume = 0.5;
        bg.play();
    
        createBounders();
        createchildren();
        createTextsBoard2();
    },
    update:function  () {

        // bad
        if(status2 == 'gameOver' && keyboard.enter.isDown) restart2();
        if(status2 == 'gameOver' && keyboard.esc.isDown) returnmenu2();
        if(status2 != 'running') return;
    
        this.physics.arcade.collide([children1, children2], platforms, effect);
        this.physics.arcade.collide([children1, children2], [leftWall, rightWall]);
        this.physics.arcade.collide(children1, children2);
        checkTouchCeiling(children1);
        checkTouchCeiling(children2);
        checkGameOver2();
    
        updatechildren();
        updatePlatforms();
        updateTextsBoard2();
    
        createPlatforms();
    }
};




function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}



function createOnePlatform () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createchildren () {
    children1 = game.add.sprite(100, 50, 'children1');
    children2 = game.add.sprite(300, 50, 'children2');

    setchildrenAttr(children1);
    setchildrenAttr(children2);
}

function setchildrenAttr(children) {
    game.physics.arcade.enable(children);
    children.body.gravity.y = 500;
    children.animations.add('left', [0, 1, 2, 3], 8);
    children.animations.add('right', [9, 10, 11, 12], 8);
    children.animations.add('flyleft', [18, 19, 20, 21], 12);
    children.animations.add('flyright', [27, 28, 29, 30], 12);
    children.animations.add('fly', [36, 37, 38, 39], 12);
    children.life = 10;
    children.unbeatableTime = 0;
    children.touchOn = undefined;
}

function createTextsBoard2 () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    var style2 = {fill: '#ffffff', fontSize: '20px'}
    text4 = game.add.text(10, 10, '', style);
    text5 = game.add.text(250, 10, '', style2);
    text6 = game.add.text(140, 200, '', style);
    text6.visible = false;
}

function updatechildren () {
    if(keyboard.left.isDown) {
        children1.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        children1.body.velocity.x = 250;
    } else {
        children1.body.velocity.x = 0;
    }

    if(keyboard.z.isDown) {
        children2.body.velocity.x = -250;
    } else if(keyboard.x.isDown) {
        children2.body.velocity.x = 250;
    } else {
        children2.body.velocity.x = 0;
    }
    setchildrenAnimate(children1);
    setchildrenAnimate(children2);
}

function setchildrenAnimate(children) {
    var x = children.body.velocity.x;
    var y = children.body.velocity.y;

    if (x < 0 && y > 0) {
        children.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        children.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        children.animations.play('left');
    }
    if (x > 0 && y == 0) {
        children.animations.play('right');
    }
    if (x == 0 && y != 0) {
        children.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      children.frame = 8;
    }
}


function updateTextsBoard2 () {
    text4.setText('player1 life:' + children1.life);
    text5.setText('player2 life:' + children2.life) ;
}


function checkGameOver2 () {
    if(children1.life <= 0 || children1.body.y > 500) {
        children1.life=0;
        gameOver2('player2');
    }
    if(children2.life <= 0 || children2.body.y > 500) {
        children2.life=0;
        gameOver2('player1');
    }
}

function gameOver2(winner) {
    bg.stop();
    die.play();
    text6.visible = true;
    text6.setText('勝利者 ' + winner + '\nEnter 重新開始'+'\nESC 回首頁');
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    status2 = 'gameOver';
}
function returnmenu2(){
    game.state.start('menu');
}

function restart2 () {
    text6.visible = false;
    distance = 0;
    createchildren();
    status2 = 'running';
}